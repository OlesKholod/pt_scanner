import psycopg2
import ssh_client

db_name = "os_data"
db_user = "notghidra"
db_password = "13245768"
db_host = "127.0.0.1"

def begin():
    conn = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_password,
        host=db_host
    )
    cur = conn.cursor()

    # Создание таблицы
    cur.execute('''
        CREATE TABLE scanner_data (
            id SERIAL PRIMARY KEY,
            address VARCHAR(50) NOT NULL,
            kernel_name VARCHAR(50),
            kernel_release VARCHAR(50),
            kernel_version VARCHAR(50),
            node_name VARCHAR(50),
            machine VARCHAR(50),
            processor VARCHAR(50),
            hardware_platform VARCHAR(50),
            operating_system VARCHAR(50)
        )
    ''')

    # Закрытие соединения
    conn.commit()
    conn.close()

def additional_db():
    #free -m; df -h; ip -a; ps aux
    pass

def load_data(iter:ssh_client.auth_data, data:ssh_client.OS_data):
    # Установка соединения с базой данных
    conn = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_password,
        host=db_host
    )

    # Создание курсора для выполнения SQL-запросов
    cur = conn.cursor()

    # SQL-запрос для вставки данных
    sql = "INSERT INTO scanner_data (address, kernel_name, kernel_release, kernel_version, node_name, machine, processor, hardware_platform, operating_system) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    data = (iter.ip, data.k_n, data.k_r, data.k_v, data.nodename, data.machine, data.processor, data.h_p, data.o_s)
    cur.execute(sql, data)

    # Подтверждение изменений и закрытие соединения
    conn.commit()
    cur.close()
    conn.close()

def delete_table_data():
    # Установка соединения с базой данных
    conn = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_password,
        host=db_host
    )

    # Создание курсора для выполнения SQL-запросов
    cur = conn.cursor()

    # SQL-запрос для вставки данных
    sql = "TRUNCATE TABLE scanner_data;"
    cur.execute(sql)

    # Подтверждение изменений и закрытие соединения
    conn.commit()
    cur.close()
    conn.close()

def get_data_from_postgres():
    # Подключение к базе данных
    conn = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_password,
        host=db_host
    )
    
    # Создание курсора
    cursor = conn.cursor()

    # Выполнение SQL-запроса
    cursor.execute("SELECT * FROM scanner_data")

    # Получение названий столбцов
    columns = [desc[0] for desc in cursor.description]

    # Получение результатов запроса
    results = cursor.fetchall()

    # Закрытие соединения к базе данных
    cursor.close()
    conn.close()

    return columns, results

def main():
    begin()

if __name__ == "__main__":
    main()