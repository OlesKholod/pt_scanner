#!/usr/bin/env python3
import ssh_client
import postgres
import argparse

def main(auth_data_file):
    print("Scanner is working")
    data = ssh_client.load_auth_data('auth_data.txt')
    x = ssh_client.mass_scan(data)
    for i in x:
        print(i)
        for key, value in i.items():
            if value != 'unknown':
                print(key, value)
                postgres.load_data(key, value)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", help="path to auth data file", required=True)
    args = parser.parse_args()
    
    if args.filename is not None:
        filename = args.filename
        main(filename)
    else:
        print("Please provide the filename as an argument")