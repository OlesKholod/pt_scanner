#!/usr/bin/env python3

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import psycopg2
from tkinter import ttk
import subprocess
import threading
import time
import sys
sys.path.append('./libs')
import ssh_client
import postgres

path_to_logfile = "./log_data/logs.txt"

def open_database_window():
    try:
        columns, results = postgres.get_data_from_postgres()
        # Создание нового окна
        db_window = tk.Toplevel()

        screen_width = db_window.winfo_screenwidth() # Получаем ширину экрана
        window_width = int(screen_width * 0.8) # Устанавливаем ширину окна (80% ширины экрана)
        window_height = 500 # Задаем высоту окна

        # Устанавливаем размер окна
        db_window.geometry(f"{window_width}x{window_height}")

        # Создание виджета таблицы
        table = ttk.Treeview(db_window, columns=columns, show="headings", height = window_height - 100)
        
        # Установка заголовков столбцов
        for col in columns:
            table.heading(col, text=col)
        
        # Добавление данных в таблицу
        for row in results:
            table.insert("", "end", values=row)
        
        # Размещение таблицы
        table.pack(fill=tk.X, expand=True)
    
    except psycopg2.Error as error:
        # Вывод сообщения об ошибке
        messagebox.showerror("Database Error", str(error))

def open_file(text):
    filepath = filedialog.askopenfilename(filetypes=[("Text Files", "*.txt")])
    
    if filepath:
        try:
            # Чтение содержимого файла
            with open(filepath, "r") as file:
                content = file.read()
                
                # Очистка текстового поля
                text.delete("1.0", "end")
                
                # Запись содержимого файла в текстовое поле
                text.insert("end", content)
        except IOError as e:
            # Вывод сообщения об ошибке
            messagebox.showerror("Error", str(e))

def show_log():
    try:
        with open(path_to_logfile, "r") as file:
            log_data = file.read()
        
        log_window = tk.Tk()
        log_window.title("Логи")
        log_text = tk.Text(log_window)
        log_text.insert(tk.END, log_data)
        log_text.config(state="disabled")
        log_text.pack(fill=tk.BOTH, expand=True)  # Растягиваем в обе стороны
        
        log_window.mainloop()
    except FileNotFoundError:
        messagebox.showerror("Ошибка", "Файл logs.txt не найден")

def start_scan1(text):
    text_content = text.get("1.0", tk.END)
    data = []
    str_data = text_content.split('\n')
    str_data.pop()
    for line in str_data:
        I = ssh_client.auth_data.from_str(line.strip())
        if I != None:
            data.append(I)
    
    x = ssh_client.mass_scan(data)
    for i in x:
        print(i)
        for key, value in i.items():
            if value != 'unknown':
                print(key, value)
                postgres.load_data(key, value)

def start_scan(text):
    # Создание окна верхнего уровня
    progress_window = tk.Toplevel()
    progress_window.title("Scan Progress")
    progress_window.geometry("300x100")

    # Создание указателя прогресса
    progressbar = ttk.Progressbar(progress_window, length=200)
    progressbar.pack(pady=10)

    def scan_complete():
        # Завершение сканирования
        messagebox.showinfo("Scan Complete", "Scan completed successfully!")

    def run_scan():
        # Запуск сканирования
        text_content = text.get("1.0", tk.END)
        data = []
        str_data = text_content.split('\n')
        str_data.pop()
        total_items = len(str_data) - 1

        for index, line in enumerate(str_data):
            I = ssh_client.auth_data.from_str(line.strip())
            if I is not None:
                data.append(I)
            progress = (index / total_items) * 100
            progressbar["value"] = progress
            progress_window.update()

        x = ssh_client.mass_scan(data)
        for i in x:
            for key, value in i.items():
                if value != 'unknown':
                    print(key, value)
                    postgres.load_data(key, value)

        # Завершение работы сканирования
        progressbar["value"] = 100
        progress_window.update()
        time.sleep(0.5)
        progress_window.destroy()
        scan_complete()

    # Запуск сканирования в отдельном потоке
    scan_thread = threading.Thread(target=run_scan)
    scan_thread.start()    

def clean_logs():
    with open(path_to_logfile, "w") as file:
        file.truncate(0)
    
def clean_db():
    postgres.delete_table_data()

def powerup_db():
    def execute_command(command):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        output, error = process.communicate()
        
        if error:
            print(f"Произошла ошибка: {error.decode()}")
        else:
            return output.decode()

    command_output = execute_command("ps -p 1 -o comm=")
    if command_output[:-1] == "systemd":
        command_output = execute_command("systemctl start postgresql")
    else:
        command_output = execute_command("service postgresql start")

def create_db():
    postgres.begin()

def main():
    # Создание главного окна
    main_window = tk.Tk()
    main_window.title("Scanner")
    main_window.geometry("1200x768")
    main_window.configure(bg="navy")
    main_window.resizable(False, False)
    # main_window.iconbitmap("./img1.ico")

    text = tk.Text(main_window, bg = "#000000", fg="white", width = 112, height = 32)
    text.grid(row=0, column=1, rowspan=5, padx=10, pady=10)

    button_frame = ttk.Frame(main_window)
    button_frame.grid(row=0, column=0, rowspan=5, padx=10, pady=10)

    open_db_button = ttk.Button(main_window, text="Открыть базу данных", command=open_database_window, width = 25)
    open_db_button.grid(row=0, column=0, padx=5, pady=5, sticky="w")

    open_button = ttk.Button(main_window, text="Открыть файл с данными", command=lambda: open_file(text), width = 25)
    open_button.grid(row=1, column=0, padx=5, pady=5, sticky="w")

    log_button = ttk.Button(main_window, text="Показать логи", command=show_log, width = 25)
    log_button.grid(row=2, column=0, padx=5, pady=5, sticky="w")

    scan_button = ttk.Button(main_window, text="Начать сканирование", command=lambda: start_scan(text), width = 25)
    scan_button.grid(row=3, column=0, padx=5, pady=5, sticky="w")

    delete_logs_button = ttk.Button(main_window, text="Очистить логи", command=clean_logs, width = 25)
    delete_logs_button.grid(row=4, column=0, padx=5, pady=5, sticky="w")

    clear_db_button = ttk.Button(main_window, text="Очистить базу данных", command=clean_db, width = 25)
    clear_db_button.grid(row=5, column=0, padx=5, pady=5, sticky="w")

    createdb_button = ttk.Button(main_window, text="Создать БД", command=create_db, width = 25)
    createdb_button.grid(row=6, column=0, padx=5, pady=5, sticky="n")
    
    powerup_button = ttk.Button(main_window, text="Включить БД", command=powerup_db, width = 25)
    powerup_button.grid(row=7, column=0, padx=5, pady=5, sticky="n")


    # Запуск главного цикла окна
    main_window.mainloop()

if __name__ == "__main__":
    main()