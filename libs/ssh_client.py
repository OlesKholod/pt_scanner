import paramiko
import threading
import fcntl
import re
from datetime import datetime

DEBUG = False
log_file = "log_data/logs.txt"

class OS_data:
    def __init__(self, args):
        self.k_n = args[0][:-1]
        self.k_r = args[1][:-1]
        self.k_v = args[2][:-1]
        self.machine = args[3][:-1]
        self.nodename = args[4][:-1]
        self.processor = args[5][:-1]
        self.h_p = args[6][:-1]
        self.o_s = args[7][:-1]
    
    def __str__(self):
        return f"Данные системы :{self.k_n}, {self.k_r}, {self.k_v}, {self.machine}, {self.nodename}, {self.processor}, {self.h_p}, {self.o_s}."

    def __repr__(self):
        return f"Объект :({self.k_n}, {self.k_r}, {self.k_v}, {self.machine}, {self.nodename}, {self.processor}, {self.h_p}, {self.o_s})"
    
class auth_data:
    def __init__(self, ip, port, username, password):
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password

    def __str__(self):
        return f"Данные системы :{self.ip}, {self.port}, {self.username}, {self.password}."

    def __repr__(self):
        return f"Данные объекта :{self.ip}, {self.port}, {self.username}, {self.password}."

    @classmethod
    def from_str(auth_data, data):
        err_message = """Вы ввели данные неправильно. Данные должны быть в формате 
            [<ip_address>\t<port_number>\t<login>\t<password>]"""
        err_message1 = "Вы неправильно ввели ip адрес"
        err_message2 = "Вы неправильно ввели номер порта"
        l_d = data.split('\t')
        print(l_d)
        if len(l_d) != 4:
            print(err_message)
            return
        if is_valid_ip(l_d[0]) == False:
            print(f"{err_message}\n{err_message1}")
            return
        if int(l_d[1]) <= 0 or int(l_d[1]) >= 65536:
            print(f"{err_message}\n{err_message2}")
            return
        return auth_data(l_d[0], int(l_d[1]), l_d[2], l_d[3])

def is_valid_ip(ip_address):
    a = ip_address.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

def load_auth_data(filename):
    data = []
    with open(filename, 'r') as file:
        for line in file:
            I = auth_data.from_str(line.strip())
            if I != None:
                data.append(I)
    return data

def log_info(data, filename):
    with open(filename, 'a') as file:
        current_datetime = datetime.now()
        fcntl.flock(file, fcntl.LOCK_EX)  # Блокировка файла
        file.write(str(current_datetime) + " : " + data + '\n')  # Запись данных в файл
        fcntl.flock(file, fcntl.LOCK_UN)  # Снятие блокировки файла

def execute_ssh_commands(ip, port, username, password, commands, data_list):
    result = []
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    log_data = ""

    try:
        ssh_client.connect(ip, port=port, username=username, password=password)
        for command in commands:
            stdin, stdout, stderr = ssh_client.exec_command(command)
            output = stdout.read().decode('utf-8')  # Получите вывод выполнения команды
            data_list.append(output)  # Запишите результат выполнения команды в список
            result.append(output)
        log_data = f"Соединение по адресу {ip}:{port} успешно."
    
    except paramiko.AuthenticationException:
        log_data = f"При попытке соединения по адресу {ip}:{port} произошла ошибка аутентификации. Проверьте логин и пароль."
        
    except paramiko.SSHException as ssh_ex:
        log_data = f"При попытке соединения по адресу {ip}:{port} произошла ошибка SSH-соединения: {str(ssh_ex)}"
        
    except Exception as ex:
        log_data = f"При попытке соединения по адресу {ip}:{port} произошла ошибка при выполнении SSH-соединения: {str(ex)}"

    finally:
        ssh_client.close()
        if DEBUG:
            print(log_data)
        log_info(log_data, log_file)
    return result


def mass_scan(data_list):
    com_list = ['uname -s', 'uname -r', 'uname -v', 'uname -n', 'uname -m', 'uname -p', 'uname -i', 'uname -o']
    result_data_list = []
    lock = threading.Lock()  # Создаем объект блокировки

    def scan_thread(data):
        thread_result = execute_ssh_commands(data.ip, data.port, data.username, data.password, com_list, [])
        if len(thread_result) == 0:
            result_data_list.append({data : "unknown"})
        else:
            with lock:  # Блокируем доступ к result_data_list
                result_data_list.append({data : OS_data(thread_result)})

    threads = []
    for data in data_list:
        thread = threading.Thread(target=scan_thread, args=(data,))
        thread.start()
        threads.append(thread)

    # Дожидаемся завершения всех потоков
    for thread in threads:
        if thread.is_alive():
            thread.join()

    return result_data_list

if __name__ == "__main__":
    current_datetime = datetime.now()

    print(str(current_datetime))
    # main()